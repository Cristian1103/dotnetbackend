﻿using MobyLabWebProgramming.Core.DataTransferObjects;
using MobyLabWebProgramming.Core.Entities;
using MobyLabWebProgramming.Core.Requests;
using MobyLabWebProgramming.Core.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyLabWebProgramming.Infrastructure.Services.Interfaces;

public interface IUserInfoService
{
    public Task<ServiceResponse<AdditionalUserInfo>> GetInfoById(Guid id, CancellationToken cancellationToken = default);

    public Task<ServiceResponse> AddUserInfo(UserInfoAddDTO info, UserDTO? requestingUser = default, CancellationToken cancellationToken = default);
    public Task<ServiceResponse> UpdateUserInfo(UserInfoUpdateDTO info, UserDTO? requestingUser = default, CancellationToken cancellationToken = default);

    public Task<ServiceResponse> DeleteUserInfo(Guid id, UserDTO? requestingUser = default, CancellationToken cancellationToken = default);
}

