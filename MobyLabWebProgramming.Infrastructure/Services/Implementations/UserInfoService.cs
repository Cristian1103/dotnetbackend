﻿using MobyLabWebProgramming.Core.Constants;
using MobyLabWebProgramming.Core.DataTransferObjects;
using MobyLabWebProgramming.Core.Entities;
using MobyLabWebProgramming.Core.Enums;
using MobyLabWebProgramming.Core.Errors;
using MobyLabWebProgramming.Core.Requests;
using MobyLabWebProgramming.Core.Responses;
using MobyLabWebProgramming.Core.Specifications;
using MobyLabWebProgramming.Infrastructure.Database;
using MobyLabWebProgramming.Infrastructure.Repositories.Interfaces;
using MobyLabWebProgramming.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MobyLabWebProgramming.Infrastructure.Services.Implementations;

public class UserInfoService : IUserInfoService
{
    private readonly IRepository<WebAppDatabaseContext> _repository;

    public UserInfoService(IRepository<WebAppDatabaseContext> repository)
    {
        _repository = repository;
    }

    public async Task<ServiceResponse<AdditionalUserInfo>> GetInfoById(Guid id, CancellationToken cancellationToken = default)
    {
        var result = await _repository.GetAsync(new UserInfoSpec(id), cancellationToken); // Get a user using a specification on the repository.

        return result != null ?
            ServiceResponse<AdditionalUserInfo>.ForSuccess(result) :
            ServiceResponse<AdditionalUserInfo>.FromError(CommonErrors.UserInfoNotFound);
    }

    public async Task<ServiceResponse> AddUserInfo(UserInfoAddDTO info, UserDTO? requestingUser, CancellationToken cancellationToken = default)
    {
        if (requestingUser != null && requestingUser.Role != UserRoleEnum.Client) // Verify who can add the user, you can change this however you se fit.
        {
            return ServiceResponse.FromError(new(HttpStatusCode.Forbidden, "Only clients can add additional info for themselves!", ErrorCodes.CannotAdd));
        }

        var result = await _repository.GetAsync(new UserInfoSpec(info.UserID), cancellationToken);

        if (result != null)
        {
            return ServiceResponse.FromError(new(HttpStatusCode.Conflict, "Additional info already exists for this user!", ErrorCodes.ProductAlreadyExists));
        }

        await _repository.AddAsync(new AdditionalUserInfo
        {
            UserID = requestingUser.Id,
            Address = info.Address,
            PhoneNumber = info.PhoneNumber,
            City = info.City,
            Country = info.Country,
            PostalCode = info.PostalCode
        }, cancellationToken); // A new entity is created and persisted in the database.

        return ServiceResponse.ForSuccess();
    }

    public async Task<ServiceResponse> UpdateUserInfo(UserInfoUpdateDTO info, UserDTO? requestingUser, CancellationToken cancellationToken = default)
    {
        if (requestingUser != null && requestingUser.Role != UserRoleEnum.Admin && requestingUser.Id != info.Id) // Verify who can add the user, you can change this however you se fit.
        {
            return ServiceResponse.FromError(new(HttpStatusCode.Forbidden, "Only the admin or the personnel can update the product!", ErrorCodes.CannotUpdate));
        }

        var entity = await _repository.GetAsync(new UserInfoSpec(requestingUser.Id), cancellationToken);

        if (entity != null) // Verify if the user is not found, you cannot update an non-existing entity.
        {
            entity.PhoneNumber = info.PhoneNumber ?? entity.PhoneNumber;
            entity.Address = info.Address ?? entity.Address;
            entity.City = info.City ?? entity.City;
            entity.Country = info.Country ?? entity.Country;
            entity.PostalCode = info.PostalCode ?? entity.PostalCode;

            await _repository.UpdateAsync(entity, cancellationToken); // Update the entity and persist the changes.
        }

        return ServiceResponse.ForSuccess();
    }

    public async Task<ServiceResponse> DeleteUserInfo(Guid id, UserDTO? requestingUser = default, CancellationToken cancellationToken = default)
    {
        var result = await _repository.GetAsync(new UserInfoSpec(id), cancellationToken);

        if (requestingUser != null && requestingUser.Role != UserRoleEnum.Admin && result != null && requestingUser.Id != result.UserID) // Verify who can add the user, you can change this however you se fit.
        {
            return ServiceResponse.FromError(new(HttpStatusCode.Forbidden, "Only the admin or the reviewer can delete a review!", ErrorCodes.CannotDelete));
        }

        await _repository.DeleteAsync<AdditionalUserInfo>(id, cancellationToken); // Delete the entity.

        return ServiceResponse.ForSuccess();
    }
}
