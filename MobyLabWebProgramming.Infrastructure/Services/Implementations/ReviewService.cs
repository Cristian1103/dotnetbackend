﻿using MobyLabWebProgramming.Core.DataTransferObjects;
using MobyLabWebProgramming.Core.Entities;
using MobyLabWebProgramming.Core.Enums;
using MobyLabWebProgramming.Core.Errors;
using MobyLabWebProgramming.Core.Requests;
using MobyLabWebProgramming.Core.Responses;
using MobyLabWebProgramming.Core.Specifications;
using MobyLabWebProgramming.Infrastructure.Database;
using MobyLabWebProgramming.Infrastructure.Repositories.Interfaces;
using MobyLabWebProgramming.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MobyLabWebProgramming.Infrastructure.Services.Implementations;

public class ReviewService : IReviewService
{
    private readonly IRepository<WebAppDatabaseContext> _repository;

    public ReviewService(IRepository<WebAppDatabaseContext> repository)
    {
        _repository = repository;
    }

    public async Task<ServiceResponse<ReviewDTO>> GetReviewById(Guid id, CancellationToken cancellationToken = default)
    {
        var result = await _repository.GetAsync(new ReviewProjectionSpec(id), cancellationToken); // Get a user using a specification on the repository.

        return result != null ?
            ServiceResponse<ReviewDTO>.ForSuccess(result) :
            ServiceResponse<ReviewDTO>.FromError(CommonErrors.ReviewNotFound);
    }

    public async Task<ServiceResponse<PagedResponse<ReviewDTO>>> GetReviewsByUserID(PaginationSearchQueryParams pagination, CancellationToken cancellationToken = default)
    {
        var result = await _repository.PageAsync(pagination, new ReviewProjectionSpec(pagination.Id, 1), cancellationToken); // Use the specification and pagination API to get only some entities from the database.

        return ServiceResponse<PagedResponse<ReviewDTO>>.ForSuccess(result);
    }

    public async Task<ServiceResponse<PagedResponse<ReviewDTO>>> GetReviewsByProductID(PaginationSearchQueryParams pagination, CancellationToken cancellationToken = default)
    {
        var result = await _repository.PageAsync(pagination, new ReviewProjectionSpec(pagination.Id, 0), cancellationToken); // Use the specification and pagination API to get only some entities from the database.

        return ServiceResponse<PagedResponse<ReviewDTO>>.ForSuccess(result);
    }

    public async Task<ServiceResponse> AddReview(ReviewAddDTO Review, UserDTO? requestingUser, CancellationToken cancellationToken = default)
    {
        if (requestingUser != null && requestingUser.Role != UserRoleEnum.Client) // Verify who can add the user, you can change this however you se fit.
        {
            return ServiceResponse.FromError(new(HttpStatusCode.Forbidden, "Only the clients can add reviews!", ErrorCodes.CannotAdd));
        }

        await _repository.AddAsync(new Review
        {
            Title = Review.Title,
            Rating = Review.Rating,
            Content = Review.Content,
            ProductID = Review.ProductID,
            UserID = requestingUser.Id,
            TimeOfUpload = DateTime.Now

        }, cancellationToken); // A new entity is created and persisted in the database.

        return ServiceResponse.ForSuccess();
    }

    public async Task<ServiceResponse> UpdateReview(ReviewUpdateDTO Review, UserDTO? requestingUser, CancellationToken cancellationToken = default)
    {
        if (requestingUser != null && requestingUser.Role != UserRoleEnum.Client && requestingUser.Id != Review.UserID) // Verify who can add the user, you can change this however you se fit.
        {
            return ServiceResponse.FromError(new(HttpStatusCode.Forbidden, "Only the reviewer can update the review!", ErrorCodes.CannotUpdate));
        }

        var entity = await _repository.GetAsync(new ReviewSpec(Review.Id), cancellationToken);

        if (entity != null) // Verify if the user is not found, you cannot update an non-existing entity.
        {
            entity.Title = Review.Title ?? entity.Title;
            entity.Rating = Review.Rating ?? entity.Rating;
            entity.Content = Review.Content ?? entity.Content;

            await _repository.UpdateAsync(entity, cancellationToken); // Update the entity and persist the changes.
        }

        return ServiceResponse.ForSuccess();
    }

    public async Task<ServiceResponse> DeleteReview(Guid id, UserDTO? requestingUser = default, CancellationToken cancellationToken = default)
    {
        var result = await _repository.GetAsync(new ReviewProjectionSpec(id), cancellationToken);

        if (requestingUser != null && requestingUser.Role != UserRoleEnum.Admin && result != null && requestingUser.Id != result.UserID) // Verify who can add the user, you can change this however you se fit.
        {
            return ServiceResponse.FromError(new(HttpStatusCode.Forbidden, "Only the admin or the reviewer can delete a review!", ErrorCodes.CannotDelete));
        }

        await _repository.DeleteAsync<Review>(id, cancellationToken); // Delete the entity.

        return ServiceResponse.ForSuccess();
    }
}
