﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MobyLabWebProgramming.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyLabWebProgramming.Infrastructure.EntityConfigurations;

public class PurchaseConfiguration : IEntityTypeConfiguration<Purchase>
{
    public void Configure(EntityTypeBuilder<Purchase> builder)
    {
        builder.Property(e => e.Id) // This specifies which property is configured.
            .IsRequired(); // Here it is specified if the property is required, meaning it cannot be null in the database.
        builder.HasKey(x => x.Id); // Here it is specifies that the property Id is the primary key.
        builder.Property(e => e.UserID)
            .IsRequired();
        builder.Property(e => e.TotalPrice)
            .IsRequired();
        builder.Property(e => e.IsCompleted)
            .IsRequired();
        builder.Property(e => e.CreatedAt)
            .IsRequired();
        builder.Property(e => e.UpdatedAt)
            .IsRequired();

        builder.HasOne(e => e.Owner) // This specifies a one-to-many relation.
            .WithMany(e => e.Purchases) // This provides the reverse mapping for the one-to-many relation. 
            .HasForeignKey(e => e.UserID) // Here the foreign key column is specified.
            .HasPrincipalKey(e => e.Id) // This specifies the referenced key in the referenced table.
            .IsRequired()
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasMany(e => e.Products)
            .WithMany(e => e.Purchases)
            .UsingEntity<ProductPurchase>(
                e => e.HasOne<Product>().WithMany().HasForeignKey(e => e.ProductID).HasPrincipalKey(e => e.Id),
                e => e.HasOne<Purchase>().WithMany().HasForeignKey(e => e.PurchaseID).HasPrincipalKey(e => e.Id));
    }
}
