﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MobyLabWebProgramming.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyLabWebProgramming.Infrastructure.EntityConfigurations;

public class AdditionalUserInfoConfiguration : IEntityTypeConfiguration<AdditionalUserInfo>
{
    public void Configure(EntityTypeBuilder<AdditionalUserInfo> builder)
    {
        builder.Property(e => e.Id) // This specifies which property is configured.
            .IsRequired(); // Here it is specified if the property is required, meaning it cannot be null in the database.
        builder.HasKey(x => x.Id); // Here it is specifies that the property Id is the primary key.
        builder.Property(e => e.UserID)
            .IsRequired();
        builder.Property(e => e.PhoneNumber)
            .HasMaxLength(255)
            .IsRequired();
        builder.Property(e => e.Address)
            .IsRequired();
        builder.Property(e => e.City)
            .HasMaxLength(255)
            .IsRequired();
        builder.Property(e => e.Country)
            .HasMaxLength(255)
            .IsRequired();
        builder.Property(e => e.PostalCode)
            .HasMaxLength(255)
            .IsRequired();
        builder.Property(e => e.CreatedAt)
            .IsRequired();
        builder.Property(e => e.UpdatedAt)
            .IsRequired();

        builder.HasOne(e => e.CurrentUser) // This specifies a one-to-many relation.
            .WithOne(e => e.UserInfo) // This provides the reverse mapping for the one-to-many relation. 
            .HasForeignKey<AdditionalUserInfo>(e => e.UserID) // Here the foreign key column is specified.
            .HasPrincipalKey<User>(e => e.Id) // This specifies the referenced key in the referenced table.
            .IsRequired()
            .OnDelete(DeleteBehavior.Cascade);
    }
}
