﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MobyLabWebProgramming.Core.DataTransferObjects;
using MobyLabWebProgramming.Core.Entities;
using MobyLabWebProgramming.Core.Enums;
using MobyLabWebProgramming.Core.Requests;
using MobyLabWebProgramming.Core.Responses;
using MobyLabWebProgramming.Infrastructure.Authorization;
using MobyLabWebProgramming.Infrastructure.Extensions;
using MobyLabWebProgramming.Infrastructure.Services.Implementations;
using MobyLabWebProgramming.Infrastructure.Services.Interfaces;

namespace MobyLabWebProgramming.Backend.Controllers;

[ApiController]
[Route("api/[controller]/[action]")]
public class AdditionalUserInfoController : AuthorizedController
{
    private readonly IUserInfoService UserInfoService;
    public AdditionalUserInfoController(IUserService userService, IUserInfoService userInfoService) : base(userService)
    {
        UserInfoService = userInfoService;
    }

    [Authorize]
    [HttpGet("{id:guid}")] // This attribute will make the controller respond to a HTTP GET request on the route /api/User/GetById/<some_guid>.
    public async Task<ActionResult<RequestResponse<AdditionalUserInfo>>> GetById([FromRoute] Guid id) // The FromRoute attribute will bind the id from the route to this parameter.
    {
        var currentUser = await GetCurrentUser();
        if (currentUser != null && currentUser.Result!=null && currentUser.Result.Role == UserRoleEnum.Client)
        {
            var resInfoCurr = await UserInfoService.GetInfoById(currentUser.Result.Id);

            return resInfoCurr.Result != null ?
                this.FromServiceResponse(resInfoCurr) :
                this.ErrorMessageResult<AdditionalUserInfo>(resInfoCurr.Error);
        }
        var resInfo = await UserInfoService.GetInfoById(id);

        return resInfo.Result != null ?
            this.FromServiceResponse(resInfo) :
            this.ErrorMessageResult<AdditionalUserInfo>(resInfo.Error);
    }

    [Authorize]
    [HttpPost] // This attribute will make the controller respond to a HTTP POST request on the route /api/User/Add.
    public async Task<ActionResult<RequestResponse>> Add([FromBody] UserInfoAddDTO info)
    {
        var currentUser = await GetCurrentUser();

        return currentUser.Result != null ?
            this.FromServiceResponse(await UserInfoService.AddUserInfo(info, currentUser.Result)) :
            this.ErrorMessageResult(currentUser.Error);
    }

    [Authorize]
    [HttpPut] // This attribute will make the controller respond to a HTTP PUT request on the route /api/User/Update.
    public async Task<ActionResult<RequestResponse>> Update([FromBody] UserInfoUpdateDTO info) // The FromBody attribute indicates that the parameter is deserialized from the JSON body.
    {
        var currentUser = await GetCurrentUser();

        return currentUser.Result != null ?
            this.FromServiceResponse(await UserInfoService.UpdateUserInfo(info, currentUser.Result)) :
            this.ErrorMessageResult(currentUser.Error);
    }

    [Authorize]
    [HttpDelete("{id:guid}")] // This attribute will make the controller respond to a HTTP DELETE request on the route /api/User/Delete/<some_guid>.
    public async Task<ActionResult<RequestResponse>> Delete([FromRoute] Guid id) // The FromRoute attribute will bind the id from the route to this parameter.
    {
        var currentUser = await GetCurrentUser();

        return currentUser.Result != null ?
            this.FromServiceResponse(await UserInfoService.DeleteUserInfo(id, currentUser.Result)) :
            this.ErrorMessageResult(currentUser.Error);
    }
}