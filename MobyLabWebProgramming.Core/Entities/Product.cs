﻿using MobyLabWebProgramming.Core.Enums;

namespace MobyLabWebProgramming.Core.Entities;

public class Product : BaseEntity
{
    public string Name { get; set; } = default!;
    public string Description { get; set; } = default!;
    public float Price { get; set; }
    public int NoInStock { get; set; }
    public Guid CategoryID { get; set; } = default!;
    public Category Category { get; set; } = default!;
    public ICollection<Review> Reviews { get; set; } = default!;
    public ICollection<Purchase> Purchases { get; set; } = default!;
}