﻿using MobyLabWebProgramming.Core.Enums;

namespace MobyLabWebProgramming.Core.Entities;

public class Review : BaseEntity
{
    public Guid UserID { get; set; } = default!;
    public Guid ProductID { get; set; } = default!;
    public int Rating { get; set; } = default!;
    public string Title { get; set; } = default!;
    public string Content { get; set; } = default!;
    public DateTime TimeOfUpload { get; set; } = default!;
    public User Reviewer { get; set; } = default!;
    public Product ReviewedProduct { get; set; } = default!;
} 