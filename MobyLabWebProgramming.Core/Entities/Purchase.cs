﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyLabWebProgramming.Core.Entities;

public class Purchase : BaseEntity
{
    public Guid UserID { get; set; } = default!;
    public float TotalPrice { get; set; } = default!;
    public bool IsCompleted { get; set; } = default!;
    public User Owner { get; set; } = default!;
    public ICollection<Product> Products { get; set; } = default!;
}