﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyLabWebProgramming.Core.Entities;

public class ProductPurchase
{
    public Guid ProductID { get; set; } = default!;
    public Guid PurchaseID { get; set; } = default!;
    public Product Product { get; set; } = default!;
    public Purchase Purchase { get; set;} = default!;
}