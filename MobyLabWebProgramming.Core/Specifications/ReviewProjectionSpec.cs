﻿using Ardalis.Specification;
using Microsoft.EntityFrameworkCore;
using MobyLabWebProgramming.Core.DataTransferObjects;
using MobyLabWebProgramming.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace MobyLabWebProgramming.Core.Specifications;

public class ReviewProjectionSpec : BaseSpec<ReviewProjectionSpec, Review, ReviewDTO>
{
    protected override Expression<Func<Review, ReviewDTO>> Spec => e => new()
    {
        Id = e.Id,
        Rating = e.Rating,
        Title = e.Title,
        Content = e.Content,
        UserID = e.UserID,
        ProductID = e.ProductID,
        TimeOfUpload = e.TimeOfUpload
    };

    public ReviewProjectionSpec(Guid id) : base(id)
    {
    }

    public ReviewProjectionSpec(Guid? id1, int idType)
    {
        switch (idType)
        {
            case 0:
                Query.Where(e => e.ProductID == id1);
                break;
            case 1:
                Query.Where(e => e.UserID == id1);
                break;
            default:
                break;
        }
    }
}
