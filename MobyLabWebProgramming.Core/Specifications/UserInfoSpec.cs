﻿using Ardalis.Specification;
using MobyLabWebProgramming.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyLabWebProgramming.Core.Specifications;

/// <summary>
/// This is a simple specification to filter the user entities from the database via the constructors.
/// Note that this is a sealed class, meaning it cannot be further derived.
/// </summary>
public sealed class UserInfoSpec : BaseSpec<UserInfoSpec, AdditionalUserInfo>
{
    public UserInfoSpec(Guid id)
    {
        Query.Where(e => e.UserID == id);
    }
}
