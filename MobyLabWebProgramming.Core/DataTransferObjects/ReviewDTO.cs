﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyLabWebProgramming.Core.DataTransferObjects;

public class ReviewDTO
{
    public Guid Id { get; set; }
    public Guid UserID { get; set; } = default!;
    public Guid ProductID { get; set; } = default!;
    public int Rating { get; set; } = default!;
    public string Title { get; set; } = default!;
    public string Content { get; set; } = default!;
    public DateTime TimeOfUpload { get; set; } = default!;
}
